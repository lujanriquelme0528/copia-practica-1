output "public_ipv4_dns" {
    description = "public IPv4 DNS of the EC2 instance"
    value       = "http://${aws_instance.web.public_dns}:8080/hello"
}