terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = var.region
}

resource "aws_s3_bucket" "daniel-tf-bucket" {
  //bucket = var.bucket
  tags = {
    //Name        = var.bucket
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_public_access_block" "bucket-public-access-block" {
  bucket = aws_s3_bucket.daniel-tf-bucket.bucket
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_object" "daniel-object" {
  bucket = aws_s3_bucket.daniel-tf-bucket.bucket
  key    = "g-hello-0.0.1-SNAPSHOT.jar"
  source = "build/libs/g-hello-11-0.0.1-SNAPSHOT.jar"
  source_hash = filemd5("build/libs/g-hello-11-0.0.1-SNAPSHOT.jar")
}

